<!DOCTYPE html>
<html lang="en">
<head>
    <title>Home</title>
</head>
<body>
    <h1> This is home page</h1>
    <x-alert>
        <x-slot:subheadding>Bold Home Page - Subheading</x-slot>
        <x-slot:title>Bold Home Page - title</x-slot>
    </x-alert>
</body>
</html>